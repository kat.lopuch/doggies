import { useQuery } from "@apollo/client";
import { setDogBreed } from "./store/features/dogSlice";
import { useAppDispatch } from "./store/store";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";
import Bone from "./images/bone-solid.svg";
import { QUERY_ALL_DOG_BREEDS } from './Queries'

const breedSelectorSx = {
  width: "270px",
  "& .MuiAutocomplete-paper": {
    backgroundColor: "#38393b",
    color: "hsl(73, 73%, 72%)",
    fontWeight: "bold",
  },
  "& .MuiAutocomplete-root": {
    backgroundColor: "#38393b",
    color: "hsl(73, 73%, 72%)",
    fontWeight: "bold",
  },
  "& .MuiAutocomplete-inputRoot": {
    fontSize: "1.5rem",
    padding: "0.2em",
    boxShadow:
      "inset 0 0 0.5em 0 hsl(73, 73%, 72%), 0 0 0.5em 0 hsl(73, 73%, 72%)",
    border: "0.125em solid hsl(73, 73%, 72%)",
    outline: "none",
    borderRadius: "0.25em",
    textDecoration: "none",
    color: "hsl(73, 73%, 72%)",
    fontFamily: "Comic Neue",
    fontWeight: "normal",
  },
  "& .MuiAutocomplete-input": {
    textShadow:
      "0 0 0.125em hsl(0 0% 100% / 0.3), 0 0 0.25em currentColor",
    fontWeight: "normal",
    textDecoration: "none",
    color: "hsl(73, 73%, 72%)",
    display: "inline-block",
    cursor: "pointer",
    fontFamily: "Comic Neue",
  },
  "& .MuiAutocomplete-popupIndicator": {
    top: "-5px",
    color: "hsl(73, 73%, 72%)",
  },
  "& .MuiAutocomplete-clearIndicator": {
    top: "-5px",
    color: "hsl(73, 73%, 72%)",
  },
}

const breedSelectorLabelSx = {
  "& .MuiInputLabel-root": {
    fontSize: "1.5rem",
    textShadow:
      "0 0 0.125em hsl(0 0% 100% / 0.3), 0 0 0.35em currentColor",
    color: "hsl(73, 73%, 72%)",
    fontFamily: "Comic Neue",
  },
  "& .MuiInputLabel-root.MuiInputLabel-shrink": {
    top: "-0.3rem",
    paddingLeft: "0.6rem",
    paddingRight: "0.6rem",
    fontSize: "1.4rem",
    color: "hsl(73, 73%, 72%)",
    boxShadow:
      "inset 0 0 0.5em 0 hsl(73, 73%, 72%), 0 0 0.5em 0 hsl(73, 73%, 72%)",
    textShadow:
      "0 0 0.125em hsl(0 0% 100% / 0.3), 0 0 0.45em currentColor",
    border: "hsl(73, 73%, 72%) 0.125em solid",
    borderRadius: "0.25em",
    textDecoration: "none",
    backgroundColor: "#282c34",
  },
}

function BreedSelector() {
  const dispatch = useAppDispatch();
  const { data: dogBreeds } = useQuery(QUERY_ALL_DOG_BREEDS);

  if (!dogBreeds)
    return (
      <img
        src={Bone}
        className={"doggies__boneSpinner doggies__loading-spinner--show"}
      />
    );

  return (
    <Autocomplete
      disablePortal
      options={dogBreeds.listAllBreeds}
      sx={breedSelectorSx}
      renderInput={(params) => (
        <TextField
          {...params}
          variant="outlined"
          sx={breedSelectorLabelSx}
          label="select breed"
        />
      )}
      onChange={(_, newValue) => {
        dispatch(setDogBreed({ breed: newValue as string }));
      }}
    />
  );
}

export default BreedSelector;
