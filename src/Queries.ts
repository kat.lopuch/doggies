import { gql } from "@apollo/client";

const QUERY_ALL_DOG_BREEDS = gql`
  query GetAllDogBreeds {
    listAllBreeds
  }
`;

const QUERY_DOG = gql`
  query GetDog {
    randomImage {
      message
    }
  }
`;

const QUERY_DOG_BY_BREED = gql`
  query GetDogByBreed($type: String!) {
    breed(type: $type) {
      message
    }
  }
`;

export { QUERY_ALL_DOG_BREEDS, QUERY_DOG, QUERY_DOG_BY_BREED };