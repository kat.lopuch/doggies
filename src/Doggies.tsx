import { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "./store/store";
import { setLoading } from "./store/features/dogSlice";
import { motion, useAnimation } from "framer-motion";
import NextDog from "./NextDog";
import BreedSelector from "./BreedSelector";
import Bone from "./images/bone-solid.svg";
import "./Doggies.css";

function Doggies() {
  const dispatch = useAppDispatch();
  const dogUrl = useAppSelector((state) => state.dog.url);
  const loading = useAppSelector((state) => state.dog.loading);
  const circularProgressClass = loading
    ? "doggies__loading-spinner--show "
    : "doggies__loading-spinner--hide";

  const controls = useAnimation();

  useEffect(() => {
    if (!loading) {
      controls.start({
        scale: [1.4, 1],
        transition: { duration: 0.2 },
      });
    }
  }, [loading]);

  return (
    <div>
      <h1 className="doggies__header ">Who is a good doggy?</h1>
      <div className="doggies__column">
        {dogUrl && (
          <motion.div animate={controls} className="doggies_imageContainer">
            <img
              className="doggies__foregroundImage "
              src={dogUrl}
              onError={(error) => {
                dispatch(setLoading(false));
              }}
              onLoad={() => {
                dispatch(setLoading(false));
              }}
            />
            <img className="doggies__ambientImage" src={dogUrl} />
          </motion.div>
        )}
        <div className="doggies__controls">
          <BreedSelector />
          <NextDog />
          <img
            src={Bone}
            className={"doggies__boneSpinner " + circularProgressClass}
          />
        </div>
      </div>
    </div>
  );
}

export default Doggies;
