import { ApolloClient, InMemoryCache, ApolloProvider } from "@apollo/client";
import Doggies from "./Doggies";
import "./App.css";

function App() {
  const client = new ApolloClient({
    cache: new InMemoryCache(),
    uri: "https://doggos-api.netlify.app/",
  });

  return (
    <ApolloProvider client={client}>
      <div className="App">
        <Doggies />
      </div>
    </ApolloProvider>
  );
}

export default App;
