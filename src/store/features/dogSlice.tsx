import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export interface Dog {
  url: string | null;
  breed: string | null;
  breeds: [string] | null;
  loading: Boolean;
}

const initialState: Dog = {
  url: null,
  breed: null,
  breeds: null,
  loading: false
};

const dogSlice = createSlice({
  name: "modal",
  initialState,
  reducers: {
    setDogUrl: (state, action: PayloadAction<{ url: string }>) => {
      state.url = action.payload.url;
    },
    setDogBreed: (state, action: PayloadAction<{ breed: string | null }>) => {
      state.breed = action.payload.breed;
    },
    setDogBreeds: (state, action: PayloadAction<{ breeds: [string] | null }>) => {
      state.breeds = action.payload.breeds;
    },
    setLoading: (state, action: PayloadAction<Boolean>) => {
      state.loading = action.payload;
    },
  },
});

export const { setDogUrl, setDogBreed, setDogBreeds, setLoading } = dogSlice.actions;
export default dogSlice.reducer;
