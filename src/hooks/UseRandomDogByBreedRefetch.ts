import { useEffect } from "react";
import { useLazyQuery } from "@apollo/client";
import { setDogUrl, setLoading } from "../store/features/dogSlice";
import { useAppDispatch } from "../store/store";
import { QUERY_DOG_BY_BREED } from '../Queries'

const useRandomDogByBreedRefetch = (dogBreed: string | null) => {
  const dispatch = useAppDispatch();

  const [
    _,
    {
      data: randomDogByBreed,
      error: randomDogByBreedError,
      loading: randomDogByBreedLoading,
      refetch: refetch,
    },
  ] = useLazyQuery(QUERY_DOG_BY_BREED, { notifyOnNetworkStatusChange: true });

  useEffect(() => {
    // Refetch in case of an error
    if (randomDogByBreedError) {
      if (dogBreed) refetch({ type: dogBreed });
      else dispatch(setLoading(false));
    }
  }, [randomDogByBreedError]);

  useEffect(() => {
    if (!randomDogByBreed) return;
    dispatch(setDogUrl({ url: randomDogByBreed.breed.message }));
  }, [randomDogByBreed]);

  useEffect(() => {
    if (randomDogByBreedLoading) dispatch(setLoading(true));
  }, [randomDogByBreedLoading]);

  return {
    refetchByBreed: () => {
      dispatch(setLoading(true));
      refetch({ type: dogBreed })
    }
  };
}

export default useRandomDogByBreedRefetch;
