import { useEffect } from "react";
import { useQuery } from "@apollo/client";
import { setDogUrl, setLoading } from "../store/features/dogSlice";
import { useAppDispatch } from "../store/store";
import { QUERY_DOG } from '../Queries'

const useRandomDogRefetch = () => {
  const dispatch = useAppDispatch();

  const { data: randomDog, loading: randomDogLoading, refetch } = useQuery(QUERY_DOG, {
    notifyOnNetworkStatusChange: true,
  });

  useEffect(() => {
    if (!randomDog) return;
    dispatch(setDogUrl({ url: randomDog.randomImage.message }));
  }, [randomDog]);

  useEffect(() => {
    if (randomDogLoading) dispatch(setLoading(true));
  }, [randomDogLoading]);

  return {
    refetch: () => {
      dispatch(setLoading(true));
      refetch();
    }
  }
}

export default useRandomDogRefetch;
