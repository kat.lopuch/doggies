import { useAppSelector } from "./store/store";
import useRandomDogRefetch from "./hooks/UseRandomDogRefetch";
import useRandomDogByBreedRefetch from "./hooks/UseRandomDogByBreedRefetch";
import "./NextDog.css";

function NextDog() {
  const dogBreed = useAppSelector((state) => state.dog.breed);
  const loading = useAppSelector((state) => state.dog.loading);

  const { refetch } = useRandomDogRefetch();
  const { refetchByBreed } = useRandomDogByBreedRefetch(dogBreed);
  const onNextDogClicked = () => {
    if (loading) return;

    if (dogBreed)
      refetchByBreed();
    else
      refetch();
  };

  const nextDogCss = loading ? "next-dog__disabled" : "next-dog";

  return (
    <a className={nextDogCss} onClick={onNextDogClicked}>
      woof woof!
    </a>
  );
}

export default NextDog;
