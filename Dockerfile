FROM node:16-alpine
WORKDIR /app/doggies
COPY . .
RUN npm install --production
CMD ["npm", "start"]